FROM node:lts-slim

ARG REPO_USERNAME
ARG REPO_PASSWORD

## Install.
RUN apt-get update && \
  apt-get install -y build-essential python git && \
  rm -rf /var/lib/apt/lists/*

## Set app directory
WORKDIR /app

COPY ./.config.projects.json /app/
COPY ./flows_cred.json /app/
COPY ./flows.json /app/
# COPY ./package-lock.json /app/
COPY ./package.json /app/
COPY ./settings.js /app/
COPY ./scripts/ /app/scripts/

RUN npm install --production

## [start] Create projects folder and fetch all project repos
RUN mkdir -p /data/projects
WORKDIR /app/projects

## Clone main workflow
RUN git clone "https://gitlab.com/ambtec/orchestrator-flows.git"
## Clone examples
# RUN git clone --branch development "https://gitlab.com/ambtec/orchestrator-flows.git"
## Example tag support and private repository access
# RUN git clone -b v0.1.0 "https://${REPO_USERNAME}:${REPO_PASSWORD}@gitlab.com/ambtec/orchestrator-flows.git"
RUN git clone "https://gitlab.com/ambtec/node-red-flow-main.git"
RUN git clone "https://gitlab.com/ambtec/node-red-flow-example-service.git"
## [end] Create projects folder and fetch all project repos

WORKDIR /app

ENV NODE_ENV=production
ENV NODE_PATH=/app/node_modules
ENV PROJECTS_PATH=/app/projects
EXPOSE ${PORT}

CMD ["npm", "start"]