const fs = require('fs');
const crypto = require('crypto');
const path = require('path');
const { parse, stringify } = require('yaml');

const nodeModulesRoot = process.env.NODE_PATH ? process.env.NODE_PATH : '/app/node_modules';
const configSource = '/app/.config.projects.json';
const projectDir = '/app/projects';
const cryptoSecret = 'acme';
const componentItems = ['schemas', 'parameters', 'securitySchemes', 'requestBodies', 'responses', 'headers', 'examples', 'links', 'callbacks'];
let allFlows = [];
let yamlBody = '';

try {
  const configRaw = fs.readFileSync(configSource, 'utf8');
  const config = JSON.parse(configRaw);
  const fallbackYamlFile = path.join(nodeModulesRoot, 'node-red-module-openapi', 'dist', 'storage', 'swagger.yaml');
  if (fs.existsSync(fallbackYamlFile)) {
    const fallbackYamlBody = fs.readFileSync(fallbackYamlFile, 'utf8');
    if (fallbackYamlBody) {
      yamlBody = parse(fallbackYamlBody);
    }
  } else {
    console.warn('Module "node-red-module-openapi" is not installed');
  }

  for (const project in config.projects) {
    if (project === config.activeProject) {
      continue;
    }

    const flowSource = `${projectDir}/${project}/flows.json`;
    const flowRaw = fs.readFileSync(flowSource, 'utf8');
    const flows = JSON.parse(flowRaw);
    allFlows = [...allFlows, ...flows];

    const swaggerSource = path.join(projectDir, project, 'swagger.yaml');
    if (fs.existsSync(swaggerSource)) {
      const swaggerRaw = fs.readFileSync(swaggerSource, 'utf8');
      const swagger = swaggerRaw ? parse(swaggerRaw) : null;
      if (swagger) {
        yamlBody.paths = {...yamlBody.paths, ...swagger.paths};
        for (const componentItem of componentItems) {
          if (!yamlBody.components[componentItem]) continue;
          if (!swagger.components[componentItem]) continue;
          yamlBody.components[componentItem] = {...yamlBody.components[componentItem], ...swagger.components[componentItem]};

        }
      }
    }
  }
  const mainProjectDir = `${projectDir}/${config.activeProject}`;
  const allFlowsOriginal = fs.readFileSync(`${mainProjectDir}/flows.json`, { encoding: 'utf8', flag: 'r' });
  const allFlowsNew = JSON.stringify(allFlows);
  const allFlowsOriginalHash = crypto.createHash('sha256', cryptoSecret).update(allFlowsOriginal).digest('hex');
  const allFlowsNewHash = crypto.createHash('sha256', cryptoSecret).update(allFlowsNew).digest('hex');

  if (allFlowsOriginalHash !== allFlowsNewHash) {
    fs.writeFileSync(`${mainProjectDir}/flows.json`, allFlowsNew, 'utf8');
  }

  const yamlBodyString = stringify(yamlBody);
  fs.writeFileSync(`${mainProjectDir}/swagger.yaml`, yamlBodyString, 'utf8');

} catch (err) {
  console.error(err);

}
