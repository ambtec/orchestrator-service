<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Orchestrator Service

Is based on [Node Red](https://nodered.org/) as low code plattform. It provide API Gateway functionalities as well as tools for easy and fast flow creation and organization of various processes, requests and events.

![Node Red Flows](https://nodered.org/images/nr-image-1.png "Node Red Flows") \
(Source: https://nodered.org/)

See [Node Red](https://nodered.org/) for more detailed informations about how it works and what other features it provides.

## How to start

You can use the [Project Infrastructure Example](https://gitlab.com/ambtec/infrastructure-example) to simply start the service in a demo environment.

Or you can run the service locally. For that a [Docker](https://www.docker.com/) installation is required.
### Build and start a local container

1. Open your console and navigate to your repository folder.
2. Build the container via `` docker build --no-cache --build-arg REPO_USERNAME=${username} --build-arg REPO_PASSWORD=${password} -t orchestrator-service . ``. The given arguments `` username `` and `` password `` are optional and only required on accessing a private repository. They are not in use right now.
3. Start the container via `` docker run -it -p 1880:1880 --env-file=.env orchestrator-service ``
4. Open a browser and navigate to the address shown in the console output, by default it is `` http://127.0.0.1:1880/admin/ `` \
In case port `` 1880 `` is already in use on Windows run: `` netstat -ano | findstr /r "1880" ``, then use the given process id to close the port `` kill [pid] ``

### Shut down a local container

1. Use `` docker ps `` to get a list of all running docker instances.
2. Find `` orchestrator-service `` and copy the container id.
3. Run `` docker kill ${CONTAINER_ID} `` to stop the container.

### Local container with custom port
1. Run `` docker ps `` to get a list of all running docker instances. \
2. Find `` orchestrator-service `` and copy the container id.
3. Run `` docker container inspect ${CONTAINER_ID} | grep -i IpAddress `` to fetch the current ip address.
4. Use this ip address to access the orchestrator `` http://${CUSTOM_IP_ADDRESS}:1880/admin/ ``

### Params
- `` PORT ``: The port Node-Red is listen on. Default: `` 1880 ``

#### ENV's ny Node-Red
- `` PRODUCTION ``: true|false - Enable NodeJS production mode. Default: `` false ``
- `` NODE_RED_ENCRYPTION_KEY ``: string|false- Add your secret to enable encryption. Default: `` false ``
- `` LOG_LEVEL ``: string - Set the log level. Default: `` INFO ``

### ENV's by dependecies

Please also make sure to add all Env's required by the loaded dependecies

- [node-red-module-graphql](https://gitlab.com/ambtec/node-red-module-graphql.git)
- [node-red-module-iam](https://gitlab.com/ambtec/node-red-module-iam.git)
- [node-red-module-openapi](https://gitlab.com/ambtec/node-red-module-openapi.git)
- [node-red-module-socketio](https://gitlab.com/ambtec/node-red-module-socketio.git)
- [ambtec-logger](https://gitlab.com/ambtec/ambtec-logger.git)

## Projects

All project endpoints are documented with OpenAPI (see [`Node Red Service OpenAPI`](https://gitlab.com/ambtec/node-red-module-openapi.git))

## License
MIT

## Credits
- https://nodered.org/docs/
- https://nodered.org/docs/getting-started/docker

