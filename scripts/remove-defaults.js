const { execSync } = require('child_process');

const files = [
  './node_modules/@node-red/nodes/core/network/10-mqtt.html',
  './node_modules/@node-red/nodes/core/network/10-mqtt.js',
  './node_modules/@node-red/nodes/core/network/22-websocket.html',
  './node_modules/@node-red/nodes/core/network/22-websocket.js',
];

try {
  const cmds = [];
  for (const file of files) {
    cmds.push(`[ -f "${file}" ] && rm ${file}`);
  }
  execSync(cmds.join(' && '));

} catch (err) {
  console.error(err);

}
